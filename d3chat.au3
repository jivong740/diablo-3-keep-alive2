#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.8.1
 Author:         GG

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

Global $ChatOn = False
Global $Delay = InputBox('Delay', 'Specify the delay between chat pings in seconds', '60')
$Delay *= 1000
Global $Message = InputBox('Chat Message', 'Type the message that you would like to send to Global Chat', 'ping')
HotKeySet("{F9}", "toggleChat")
HotKeySet("{F10}", "exitScript")

While 1
	Sleep(1000)
WEnd

Func exitScript()
	Exit
EndFunc

Func toggleChat()
	$ChatOn = Not $ChatOn
	If $ChatOn Then
		sendChat()
	EndIf
EndFunc

Func sendChat()
	While $ChatOn
		Send("{ENTER}" & $Message & "{ENTER}")
		Sleep($Delay)
	Wend
EndFunc
